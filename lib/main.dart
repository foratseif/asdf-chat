import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:web_socket_channel/io.dart';
import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final title = 'WebSocket Demo';
    return MaterialApp(
      title: title,
      home: MyHomePage(
        title: title,
        channel: IOWebSocketChannel.connect('ws://gogi.gotdns.ch:6969/socket.io/?EIO=3&transport=websocket'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;
  final WebSocketChannel channel;

  MyHomePage({Key key, @required this.title, @required this.channel})
      : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Form(
              child: TextFormField(
                controller: _controller,
                decoration: InputDecoration(labelText: 'Send a message'),
              ),
            ),
            StreamBuilder(
              stream: widget.channel.stream,
              builder: (context, snapshot) {

                print("DATA: (${snapshot.data})");
                if (snapshot.hasError)
                  return Text('Error: ${snapshot.error}');


                switch (snapshot.connectionState) {
                  case ConnectionState.none: return Text('none');
                  case ConnectionState.waiting: return Text('waiting...');
                }

                String shit = snapshot.data.substring(0,2);
                print(shit);
                if(shit != "42") return Text('not 42');

                List packet = jsonDecode(snapshot.data.substring(2, snapshot.data.length));
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 24.0),
                  child: Text(snapshot.hasData ? '${packet[1]}' : ''),
                );

                //return Padding(
                //  padding: const EdgeInsets.symmetric(vertical: 24.0),
                //  child: Text(snapshot.hasData ? '${snapshot.data[1]}' : ''),
                //);
              },
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _sendMessage,
        tooltip: 'Send message',
        child: Icon(Icons.send),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void _sendMessage() {
    if (_controller.text.isNotEmpty) {
      //List<String> packet = ["chat message", _controller.text];
      widget.channel.sink.add("42[\"msg\",\"${_controller.text}\"]");
    }
  }

  @override
  void dispose() {
    widget.channel.sink.close();
    super.dispose();
  }
}

